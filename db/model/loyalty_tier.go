package model

type LoyaltyTier struct {
	ID     int32 `gorm:"primaryKey"`
	Name   string
	Weight int
}
