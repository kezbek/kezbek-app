package model

import (
	"time"

	"github.com/shopspring/decimal"
)

type Transaction struct {
	ID                int64 `gorm:"primaryKey"`
	PartnerCode       string
	UserID            int64
	UserEmail         string
	TotalProduct      int64
	TotalPrice        decimal.Decimal `gorm:"type:numeric;"`
	WalletPartnerCode string
	CashbackAmount    decimal.Decimal `gorm:"type:numeric;"`
	CreatedAt         time.Time
	UpdatedAt         time.Time
}
