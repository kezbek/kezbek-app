package model

type Promo struct {
	ID               int64 `gorm:"primaryKey"`
	TotalProduct     int64
	TotalPriceGt     float64
	TotalPriceLt     float64
	RewardAmount     float64
	RewardPercentage float64
	Status           int
}
