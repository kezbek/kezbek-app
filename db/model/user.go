package model

type User struct {
	ID                  int64 `gorm:"primaryKey"`
	PhoneNo             string
	LoyaltyTierID       int
	TierPurchaseCounter int
}
