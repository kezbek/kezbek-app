package model

type LoyaltyReward struct {
	ID             int32 `gorm:"primaryKey"`
	TierID         int32
	PurchaseNumber int32
	Amount         float64
	Tier           LoyaltyTier
	IsMax          bool
}
