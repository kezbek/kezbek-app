-- migrate:up
create table promos (
  id SERIAL PRIMARY KEY,
  total_product INT NOT NULL,
  total_price_min INT NOT NULL,
  total_price_max INT NOT NULL,
  reward_amount NUMERIC,
  reward_percentage NUMERIC,
  status SMALLINT DEFAULT 0
);

-- migrate:down
drop table if exists promos;
