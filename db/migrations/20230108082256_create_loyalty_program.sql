-- migrate:up
create table loyalty_tiers (
  id SERIAL PRIMARY KEY,
  name VARCHAR (25) NOT NULL,
  weight INT NOT NULL DEFAULT 0
);

create table loyalty_rewards (
  id SERIAL PRIMARY KEY,
  tier_id INT,
  purchase_number INT,
  amount DECIMAL(8,3),
  is_max BOOLEAN DEFAULT false
);


-- migrate:down
drop table if exists loyalty_rewards;
drop table if exists loyalty_tiers;
