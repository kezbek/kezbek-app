-- migrate:up
create table transactions (
  id BIGSERIAL PRIMARY KEY,
  partner_code VARCHAR (30) NOT NULL,
  user_id BIGINT NOT NULL,
  user_email VARCHAR(50) NULL,
  total_product INT NOT NULL,
  total_price NUMERIC NOT NULL,
  wallet_partner_code VARCHAR(15) NOT NULL,
  cashback_amount NUMERIC,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL
);


-- migrate:down
drop table if exists transactions;
