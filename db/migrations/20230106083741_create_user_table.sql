-- migrate:up
create table users (
  id BIGSERIAL PRIMARY KEY,
  phone_no VARCHAR (25) NOT NULL,
  loyalty_tier_id INT NOT NULL,
  tier_purchase_counter INT NOT NULL DEFAULT 1
);

create index "user_phone_no_index" on users (phone_no);

-- migrate:down
drop index if exists "user_phone_no_index";
drop table if exists users;
