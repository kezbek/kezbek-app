.PHONY: test

lint:
	golangci-lint run ./... --color always --timeout 10m

sec:
	gosec ./... 

test-cover:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go test `go list ./... | grep -v /cmd | grep -v /db | grep -v /docs | grep -v /domain | grep -v config | grep -v mocks` -coverprofile coverage.txt
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go tool cover -func ./coverage.txt

test:
	./test/run.sh

tidy:
	go mod tidy

run:
	go run ./cmd/api.go

build:
	go build -o main ./cmd/api.go