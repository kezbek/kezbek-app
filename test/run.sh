#!/bin/bash

ALLPACKAGES=$(go list ./... | grep -Ev "scripts")
DISCARDS=(
    "kezbek/cmd"
    "kezbek/db/model"
    "kezbek/docs"
    "kezbek/domain"
    "kezbek/internal/config"
    "kezbek/internal/mocks"
)
PACKAGES=""

# Remove discarded packages
for PKG in ${ALLPACKAGES[@]}; do
    CONTINUE="0"
    for DISCARD in ${DISCARDS[@]}; do
        if [ "$PKG" == "$DISCARD" ]; then
            CONTINUE="1"
        fi
    done

    if [ "$CONTINUE" == "1" ]; then
        continue
    fi

    if [ "$PACKAGES" == "" ]; then
        PACKAGES="${PKG}"
    else
        PACKAGES="${PACKAGES},${PKG}"
    fi
done

CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go test ./... -p 1 -v -coverpkg=$PACKAGES -coverprofile=coverage.txt -covermode=atomic
