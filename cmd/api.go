package main

import (
	"kezbek/internal/config"
	"kezbek/internal/handler"
	"kezbek/internal/repository"
	"kezbek/internal/usecase"
	"os"

	_ "kezbek/docs"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/joho/godotenv"
	swagger "github.com/swaggo/fiber-swagger"
	"go.uber.org/zap"
)

// @swagger         2.0
// @title           Kezbek Service Backend API docs
// @version         2.0
// @description     This api docs can be accessed as a sandbox which can be accessed only in development environment only.
// @termsOfService  http://swagger.io/terms/

// @contact.name   API Support
// @contact.url    https://kezbek.com
// @contact.email  suppport@kezbek.com

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html

// @host      localhost:6001
func main() {
	loadEnv()
	baseUrlPath := os.Getenv("BASE_URL_PATH")

	base := config.NewBaseDep(os.Getenv("APP_NAME"))
	db, err := config.NewDBPool(base.Logger)
	if err != nil {
		os.Exit(1)
	}

	userRepo := repository.NewUserRepo(&repository.User{
		Db:   db,
		Base: base,
	})
	transactionRepo := repository.NewTransactionRepo(&repository.Transaction{
		Db:   db,
		Base: base,
	})
	loyaltyRepo := repository.NewLoyaltyRepo(&repository.Loyalty{
		Db:   db,
		Base: base,
	})
	promoRepo := repository.NewPromoRepo(&repository.Promo{
		Db:   db,
		Base: base,
	})

	userUsecase := usecase.NewUserUsecase(&usecase.User{
		Base:        base,
		UserRepo:    userRepo,
		LoyaltyRepo: loyaltyRepo,
	})
	transactionUsecase := usecase.NewUTransactionUsecase(&usecase.Transaction{
		Base:        base,
		Repo:        transactionRepo,
		UserUsecase: userUsecase,
		LoyaltyRepo: loyaltyRepo,
		PromoRepo:   promoRepo,
	})

	app := fiber.New()
	app.Use(config.Request(base.Logger), cors.New())
	app.Get("healthz", func(c *fiber.Ctx) error {
		return c.JSON("Healthy")
	})
	app.Get(baseUrlPath+"/swagger/*", swagger.WrapHandler)

	partner := app.Group("/api/_partners")
	partner.Use(config.BasicAuthMiddleware(base.Logger))

	handler.TransactionHandler(partner, handler.Transaction{
		Base:               base,
		TransactionUsecase: transactionUsecase,
	})

	err = app.Listen(":" + os.Getenv("APP_PORT"))
	if err != nil {
		base.Logger.Error("failed running server", zap.Error(err))
	}
}

func loadEnv() {
	_, err := os.Stat(".env")
	if err == nil {
		err = godotenv.Load()
		if err != nil {
			panic("failed load env file")
		}
	}
}
