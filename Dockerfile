FROM golang:1.19-alpine3.16 AS builder

ARG source_path
ARG service_name
ARG ci_job_token

RUN apk --update add git less openssh && \
    apk add --no-cache ca-certificates && \
    update-ca-certificates

WORKDIR $GOPATH/src/$CI_PROJECT_DIR
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go mod tidy && go build -ldflags="-w -s" -o /main cmd/api.go

FROM alpine:3.16
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /main /main
ARG env
ENV ENVIRONMENT $env

EXPOSE 6001
CMD [ "/app/main" ]