package domain

type TransactionRequest struct {
	PartnerCode       string  `swaggerignore:"true"`
	UserEmail         string  `json:"user_email"`
	UserPhoneNo       string  `json:"user_phone_no"`
	TotalProduct      int     `json:"total_product"`
	TotalPrice        float64 `json:"total_price"`
	WalletPartnerCode string  `json:"wallet_partner_code"`
}
