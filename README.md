# Kezbek App

## Service Diagram
### High Level Architecture
![Image](docs/diagrams/high-level-arch.drawio.png)

---
### ERD
![Image](docs/diagrams/kezbek-app-erd.drawio.png)

---

## Usecase Assumptions
- User with the same msisdn from different partner will be treated as the same user, since most e-wallet provider using msisdn as their unique user identification
- Type 1 Cashback will always will be applied for every request comes to kezbek, assuming partner is the one handling which transaction need casback from kezbek

---

## Setup Locally
### prerequisite
- Golang 1.19
- PostgreSQL 
- Redis
- Docker
- Make
- [DBMate](https://github.com/amacneil/dbmate)
### run
```bash
cp env.sample .env # modify env value to your need

dbmate create # create database
dbmate migrate

go mod tidy
go run main.go
```
