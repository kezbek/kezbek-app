package usecase

import (
	"kezbek/db/model"
	"kezbek/internal/config"
	"kezbek/internal/repository"

	"go.uber.org/zap"
)

type User struct {
	Base        *config.BaseDep
	UserRepo    repository.UserRepository
	LoyaltyRepo repository.LoyaltyRepo
}

type UserUsecase interface {
	GetUser(id int64) *model.User
	GetOrCreateUser(phoneNo string) (user *model.User, err error)
	UpgradeUser(user model.User) error
	UpdateUser(user model.User) error
}

func NewUserUsecase(usecase *User) UserUsecase {
	return usecase
}

func (u *User) GetUser(id int64) *model.User {
	user, err := u.UserRepo.GetUserByID(id)
	if err != nil {
		u.Base.Logger.Error("error usecase get user", zap.Error(err))
	}
	return user
}

func (u *User) GetOrCreateUser(phoneNo string) (user *model.User, err error) {
	userMod, err := u.UserRepo.GetUserByPhoneNo(phoneNo)
	if err != nil {
		u.Base.Logger.Error("error usecase get user", zap.Error(err))
		return user, err
	} else if userMod == nil {
		tier := u.LoyaltyRepo.GetDefaultLoyaltyTier()
		userMod := &model.User{
			PhoneNo:             phoneNo,
			TierPurchaseCounter: 0,
			LoyaltyTierID:       int(tier.ID),
		}
		_, err := u.UserRepo.CreateUser(userMod)
		if err != nil {
			return user, err
		}
		user = userMod
	} else {
		user = userMod
	}

	return user, nil
}

func (u *User) UpgradeUser(user model.User) error {
	tierBefore := user.LoyaltyTierID
	tiers := u.LoyaltyRepo.GetAllLoyatyTiers()
	for i, tier := range tiers {
		if tier.ID == int32(tierBefore) {
			if i == len(tiers)-1 {
				break
			} else {
				user.LoyaltyTierID = int(tiers[i+1].ID)
				user.TierPurchaseCounter = 0
				break
			}
		}
	}

	if tierBefore != user.LoyaltyTierID {
		err := u.UpdateUser(user)
		if err != nil {
			return err
		}
	}

	return nil
}

func (u *User) UpdateUser(user model.User) error {
	err := u.UserRepo.UpdateUser(&user)
	if err != nil {
		u.Base.Logger.Error("usecase - failed upgrade user", zap.Error(err))
		return err
	}
	return nil
}
