package usecase

import (
	"errors"
	"kezbek/db/model"
	"kezbek/internal/config"
	"kezbek/internal/mocks"
	"kezbek/internal/repository"
	"reflect"
	"testing"

	"github.com/stretchr/testify/mock"
	"gorm.io/gorm"
)

func TestUser_GetUser(t *testing.T) {
	user := &model.User{
		ID:      1,
		PhoneNo: "+628111111111",
	}

	loggerMock := new(mocks.LoggerConf)
	userRepoMock := new(mocks.UserRepository)
	loyaltyRepoMock := new(mocks.LoyaltyRepo)

	loggerMock.On("Error", mock.Anything, mock.Anything)

	fieldBase := fields{
		UserRepo:    userRepoMock,
		LoyaltyRepo: loyaltyRepoMock,
		Base: &config.BaseDep{
			Logger: loggerMock,
		},
	}

	tests := []struct {
		name         string
		fields       fields
		args         int64
		want         *model.User
		userRepoCall *mock.Call
	}{
		{
			name:   "success",
			fields: fieldBase,
			args:   1,
			want:   user,
			userRepoCall: userRepoMock.
				On("GetUserByID", user.ID).
				Once().
				Return(user, nil),
		},
		{
			name:   "failed user not exist",
			fields: fieldBase,
			args:   2,
			want:   nil,
			userRepoCall: userRepoMock.
				On("GetUserByID", int64(2)).
				Once().
				Return(nil, gorm.ErrRecordNotFound),
		},
		{
			name:   "failed other error",
			fields: fieldBase,
			args:   3,
			want:   nil,
			userRepoCall: userRepoMock.
				On("GetUserByID", int64(3)).
				Once().
				Return(nil, errors.New("other error")),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{
				Base:        tt.fields.Base,
				UserRepo:    tt.fields.UserRepo,
				LoyaltyRepo: tt.fields.LoyaltyRepo,
			}

			got := u.GetUser(tt.args)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("User.GetUser() = %v, want %v", got, tt.want)
			}
		})
	}
}

type fields struct {
	Base        *config.BaseDep
	UserRepo    repository.UserRepository
	LoyaltyRepo repository.LoyaltyRepo
}

func TestUser_GetOrCreateUser(t *testing.T) {
	user := &model.User{
		ID:      1,
		PhoneNo: "+628111111111",
	}

	loyalty := model.LoyaltyTier{
		ID:     1,
		Name:   "Bronze",
		Weight: 0,
	}

	userCreate := &model.User{
		PhoneNo:             "+628111111112",
		TierPurchaseCounter: 0,
		LoyaltyTierID:       int(loyalty.ID),
	}

	loggerMock := new(mocks.LoggerConf)
	userRepoMock := new(mocks.UserRepository)
	loyaltyRepoMock := new(mocks.LoyaltyRepo)

	loggerMock.On("Error", mock.Anything, mock.Anything)

	fieldBase := fields{
		UserRepo:    userRepoMock,
		LoyaltyRepo: loyaltyRepoMock,
		Base: &config.BaseDep{
			Logger: loggerMock,
		},
	}

	tests := []struct {
		name            string
		fields          fields
		args            string
		wantUser        *model.User
		wantErr         bool
		userGetCall     *mock.Call
		userCreateCall  *mock.Call
		loyaltyRepoCall *mock.Call
	}{
		{
			name:     "success user found",
			fields:   fieldBase,
			args:     "+628111111111",
			wantUser: user,
			wantErr:  false,
			userGetCall: userRepoMock.
				On("GetUserByPhoneNo", "+628111111111").
				Once().
				Return(user, nil),
		},
		{
			name:     "success user not found, create user",
			fields:   fieldBase,
			args:     userCreate.PhoneNo,
			wantUser: userCreate,
			wantErr:  false,
			userGetCall: userRepoMock.
				On("GetUserByPhoneNo", userCreate.PhoneNo).
				Once().
				Return(nil, nil),
			userCreateCall: userRepoMock.
				On("CreateUser", userCreate).
				Once().
				Return(user, nil),
			loyaltyRepoCall: loyaltyRepoMock.
				On("GetDefaultLoyaltyTier").
				Once().
				Return(loyalty),
		},
		{
			name:     "failed create user",
			fields:   fieldBase,
			args:     userCreate.PhoneNo,
			wantUser: nil,
			wantErr:  true,
			userGetCall: userRepoMock.
				On("GetUserByPhoneNo", userCreate.PhoneNo).
				Once().
				Return(nil, nil),
			userCreateCall: userRepoMock.
				On("CreateUser", userCreate).
				Once().
				Return(nil, errors.New("other create user error")),
			loyaltyRepoCall: loyaltyRepoMock.
				On("GetDefaultLoyaltyTier").
				Once().
				Return(loyalty),
		},
		{
			name:     "failed other error",
			fields:   fieldBase,
			args:     "+628111111113",
			wantUser: nil,
			wantErr:  true,
			userGetCall: userRepoMock.
				On("GetUserByPhoneNo", "+628111111113").
				Once().
				Return(nil, errors.New("other find user error")),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{
				Base:        tt.fields.Base,
				UserRepo:    tt.fields.UserRepo,
				LoyaltyRepo: tt.fields.LoyaltyRepo,
			}
			gotUser, err := u.GetOrCreateUser(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("User.GetOrCreateUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotUser, tt.wantUser) {
				t.Errorf("User.GetOrCreateUser() = %v, want %v", gotUser, tt.wantUser)
			}
		})
	}
}

func TestUser_UpgradeUser(t *testing.T) {
	type fields struct {
		Base        *config.BaseDep
		UserRepo    repository.UserRepository
		LoyaltyRepo repository.LoyaltyRepo
	}
	type args struct {
		user model.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{
				Base:        tt.fields.Base,
				UserRepo:    tt.fields.UserRepo,
				LoyaltyRepo: tt.fields.LoyaltyRepo,
			}
			if err := u.UpgradeUser(tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("User.UpgradeUser() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUser_UpdateUser(t *testing.T) {
	type fields struct {
		Base        *config.BaseDep
		UserRepo    repository.UserRepository
		LoyaltyRepo repository.LoyaltyRepo
	}
	type args struct {
		user model.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &User{
				Base:        tt.fields.Base,
				UserRepo:    tt.fields.UserRepo,
				LoyaltyRepo: tt.fields.LoyaltyRepo,
			}
			if err := u.UpdateUser(tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("User.UpdateUser() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
