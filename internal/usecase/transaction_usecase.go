package usecase

import (
	"fmt"
	"kezbek/db/model"
	"kezbek/domain"
	"kezbek/internal/config"
	"kezbek/internal/repository"

	"github.com/shopspring/decimal"
	"go.uber.org/zap"
)

type Transaction struct {
	Base        *config.BaseDep
	Repo        repository.TransactionRepo
	UserUsecase UserUsecase
	LoyaltyRepo repository.LoyaltyRepo
	PromoRepo   repository.PromoRepo
}

type TransactionUsecase interface {
	CreateTransaction(req domain.TransactionRequest) (*model.Transaction, error)
	calculateCashback(req domain.TransactionRequest, user model.User) (float64, bool)
	calculateLoyaltyAmount(req domain.TransactionRequest, user model.User) (float64, bool)
}

func NewUTransactionUsecase(usecase *Transaction) TransactionUsecase {
	return usecase
}

func (u *Transaction) CreateTransaction(req domain.TransactionRequest) (*model.Transaction, error) {
	var transaction *model.Transaction
	phoneNo := "+" + req.UserPhoneNo
	user, err := u.UserUsecase.GetOrCreateUser(phoneNo)
	if err != nil {
		u.Base.Logger.Error("failed create transaction", zap.Error(err))
		return transaction, err
	}

	user.TierPurchaseCounter += 1

	cashbackAmount, needUpgrade := u.calculateCashback(req, *user)
	totalPriceConv, _ := decimal.NewFromString(fmt.Sprintf("%.3f", req.TotalPrice))
	cashbackAmmountConv, _ := decimal.NewFromString(fmt.Sprintf("%.3f", cashbackAmount))

	u.Base.Logger.Info("CASHBACK", zap.Any("totalAmount", cashbackAmount))
	u.Base.Logger.Info("CASHBACK", zap.Any("totalAmountConv", cashbackAmmountConv))

	transaction = &model.Transaction{
		PartnerCode:       req.PartnerCode,
		UserID:            user.ID,
		UserEmail:         req.UserEmail,
		TotalProduct:      int64(req.TotalProduct),
		TotalPrice:        totalPriceConv,
		WalletPartnerCode: req.WalletPartnerCode,
		CashbackAmount:    cashbackAmmountConv,
	}
	u.Base.Logger.Info("DEBUG", zap.Any("transactions", transaction))

	err = u.Repo.CreateTransaction(transaction)
	if err != nil {
		u.Base.Logger.Error("usecase - failed create transaction", zap.Error(err))
		return transaction, err
	}

	if needUpgrade {
		go func() {
			err := u.UserUsecase.UpgradeUser(*user)
			if err != nil {
				u.Base.Logger.Error("Failed Upgrade User")
			}
		}()
	} else {
		go func() {
			err := u.UserUsecase.UpdateUser(*user)
			if err != nil {
				u.Base.Logger.Error("Failed Update User")
			}
		}()
	}

	if cashbackAmount > 0 {
		// TODO trigger pubsub email
	}

	return transaction, nil
}

func (u *Transaction) calculateCashback(req domain.TransactionRequest, user model.User) (float64, bool) {
	var totalAmount float64
	promoAmount := u.PromoRepo.GetPromoAmount(int64(req.TotalProduct), req.TotalPrice)
	u.Base.Logger.Info("CASHBACK", zap.Any("promo", promoAmount))

	loyaltyAmount, isMax := u.calculateLoyaltyAmount(req, user)
	u.Base.Logger.Info("CASHBACK", zap.Any("loyalty", loyaltyAmount))
	u.Base.Logger.Info("CASHBACK", zap.Any("ismax", isMax))

	totalAmount = promoAmount + loyaltyAmount
	return totalAmount, isMax
}

func (u *Transaction) calculateLoyaltyAmount(req domain.TransactionRequest, user model.User) (float64, bool) {
	reward, err := u.LoyaltyRepo.GetLoyaltyReward(int32(user.TierPurchaseCounter), int32(user.LoyaltyTierID))
	if err != nil {
		u.Base.Logger.Error("usecase - failed calculated", zap.Error(err))
		return 0, false
	}

	return reward.Amount, reward.IsMax
}
