package handler

import (
	"kezbek/domain"
	"kezbek/internal/config"
	"kezbek/internal/usecase"
	"net/http"

	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
)

type Transaction struct {
	Base               *config.BaseDep
	TransactionUsecase usecase.TransactionUsecase
}

func NewTransactionHandler(transaction Transaction) *Transaction {
	return &transaction
}

func TransactionHandler(router fiber.Router, transaction Transaction) {
	handler := NewTransactionHandler(transaction)
	router.Post("/transactions", handler.CreateTransaction)
}

// @Tags Transaction APIs
// API Create Transaction from partner
// @Summary Create Transaction from partner
// @Schemes
// @Accept json
// @Produce json
// @Param Authorization header string true "Your Token to Access" default(Basic )
// @Param x-partner-code  header string true "Partner code" Enums(BUKATOKO, LAPAKPEDIA)
// @Param request body domain.TransactionRequest true "delete bulk promotion"
// @Success 200
// @Failure 404
// @Failure 500
// @Router /api/_partners/transactions [post]
func (h *Transaction) CreateTransaction(ctx *fiber.Ctx) error {
	var req domain.TransactionRequest

	if err := ctx.BodyParser(&req); err != nil {
		h.Base.Logger.Error("handler - failed parsing request", zap.Error(err))
		return ctx.Status(http.StatusBadRequest).JSON(config.BaseResponse{
			Meta: config.MetaResponse{
				Code:    config.ERR_GENERAL_BAD_REQUEST,
				Message: "failed parsing",
			},
		})
	} else {
		req.PartnerCode = ctx.Get(config.REQ_HEADER_PARTNER)
		transaction, err := h.TransactionUsecase.CreateTransaction(req)
		if err != nil {
			return ctx.Status(http.StatusBadRequest).JSON(config.BaseResponse{
				Meta: config.MetaResponse{
					Code:    config.ERR_GENERAL_BAD_REQUEST,
					Message: "bad request",
				},
			})
		} else {
			return ctx.Status(http.StatusOK).JSON(config.BaseResponse{
				Data: transaction,
				Meta: config.MetaResponse{
					Code:    config.SUCCESS_CODE,
					Message: "success",
				},
			})
		}
	}
}
