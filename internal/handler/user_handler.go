package handler

import (
	"kezbek/internal/config"
	"kezbek/internal/usecase"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

type User struct {
	Base        *config.BaseDep
	UserUsecase *usecase.User
}

func NewUserHandler(user User) *User {
	return &user
}

func UserHandler(router fiber.Router, user User) {
	handler := NewUserHandler(user)
	router.Get("/:id", handler.GetUser)
}

// @Tags User APIs
// API Get user by id
// @Summary get user by id and return detail user
// @Schemes
// @Accept json
// @Produce json
// @Param Authorization header string true "Your Token to Access" default(Basic )
// @Param x-partner-code  header string true "Partner code" Enums(BUKATOKO, LAPAKPEDIA)
// @Param id path int true "User ID"
// @Success 200
// @Failure 404
// @Failure 500
// @Router /api/_partners/{id} [get]
func (h *User) GetUser(ctx *fiber.Ctx) error {
	id, _ := ctx.ParamsInt("id")
	user := h.UserUsecase.GetUser(int64(id))
	if user != nil {
		return ctx.Status(http.StatusOK).JSON(config.BaseResponse{
			Data: user,
			Meta: config.MetaResponse{
				Code:    2001,
				Message: "success",
			},
		})
	} else {
		return ctx.Status(http.StatusBadRequest).JSON(config.BaseResponse{
			Data: nil,
			Meta: config.MetaResponse{
				Code:    4041,
				Message: "user not found",
			},
		})
	}
}
