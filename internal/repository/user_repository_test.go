package repository

import (
	"kezbek/db/model"
	"kezbek/internal/config"
	"reflect"
	"testing"

	"gorm.io/gorm"
)

// type Case struct {
// 	description string
// 	input       string
// 	expectation string
// }

// type Suite struct {
// 	suite.Suite
// 	DB         *gorm.DB
// 	mock       sqlmock.Sqlmock
// 	Base       *config.BaseDep
// 	repository User
// }

// func SetupSuite() error {
// 	s := &Suite{}
// 	var (
// 		sqlDb *sql.DB
// 		err   error
// 	)

// 	sqlDb, s.mock, err = sqlmock.New()
// 	if err != nil {
// 		return err
// 	}

// 	s.DB, err = gorm.Open(postgres.New(postgres.Config{
// 		DSN:  "postgres",
// 		Conn: sqlDb,
// 	}))
// 	if err != nil {
// 		return err
// 	}

// 	s.DB.Logger = logger.Default.LogMode(logger.Info)

// 	// s.repository = NewUserRepo()
// 	return nil
// }

// func (s *Suite) TestUser_GetUserByID() {
// 	s.mock.ExpectQuery(
// 		regexp.QuoteMeta("SELECT * FROM USERS")).
// 		WillReturnRows(
// 			sqlmock.NewRows([]string{"id", "name"}).
// 				AddRow("1", "kekeke"),
// 		)

// 	res, err := s.repository.GetUserByID(10)
// 	require.NoErrorf(s.mock, err, "test")

// 	require.Nil(s.mock, reflect.DeepEqual(&model.User{ID: 1, PhoneNo: "name"}, res))
// }

// func TestUser_GetUserByID(t *testing.T) {

// 	type fields struct {
// 		Db   *gorm.DB
// 		Base *config.BaseDep
// 	}
// 	type args struct {
// 		id int64
// 	}
// 	tests := []struct {
// 		name     string
// 		fields   fields
// 		args     args
// 		wantUser *model.User
// 		wantErr  bool
// 	}{
// 		{
// 			name: "success",
// 			fields: fields{
// 				Db: gormDb,
// 				Base: &config.BaseDep{
// 					Logger: &config.Logger{
// 						Dep: zap.NewExample(),
// 					},
// 				},
// 			},
// 		},
// 	}

// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			r := &User{
// 				Db:   tt.fields.Db,
// 				Base: tt.fields.Base,
// 			}

// 			gotUser, err := r.GetUserByID(tt.args.id)
// 			if (err != nil) != tt.wantErr {
// 				t.Errorf("User.GetUserByID() error = %v, wantErr %v", err, tt.wantErr)
// 				return
// 			}

// 			if !reflect.DeepEqual(gotUser, tt.wantUser) {
// 				t.Errorf("User.GetUserByID() = %v, want %v", gotUser, tt.wantUser)
// 			}
// 		})
// 	}
// }

func TestUser_GetUserByPhoneNo(t *testing.T) {
	type fields struct {
		Db   *gorm.DB
		Base *config.BaseDep
	}
	type args struct {
		phoneNo string
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		wantUser *model.User
		wantErr  bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &User{
				Db:   tt.fields.Db,
				Base: tt.fields.Base,
			}
			gotUser, err := r.GetUserByPhoneNo(tt.args.phoneNo)
			if (err != nil) != tt.wantErr {
				t.Errorf("User.GetUserByPhoneNo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotUser, tt.wantUser) {
				t.Errorf("User.GetUserByPhoneNo() = %v, want %v", gotUser, tt.wantUser)
			}
		})
	}
}

func TestUser_CreateUser(t *testing.T) {
	type fields struct {
		Db   *gorm.DB
		Base *config.BaseDep
	}
	type args struct {
		u *model.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *model.User
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &User{
				Db:   tt.fields.Db,
				Base: tt.fields.Base,
			}
			got, err := r.CreateUser(tt.args.u)
			if (err != nil) != tt.wantErr {
				t.Errorf("User.CreateUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("User.CreateUser() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUser_UpdateUser(t *testing.T) {
	type fields struct {
		Db   *gorm.DB
		Base *config.BaseDep
	}
	type args struct {
		u *model.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &User{
				Db:   tt.fields.Db,
				Base: tt.fields.Base,
			}
			if err := r.UpdateUser(tt.args.u); (err != nil) != tt.wantErr {
				t.Errorf("User.UpdateUser() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
