package repository

import (
	"kezbek/db/model"
	"kezbek/internal/config"

	"go.uber.org/zap"
	"gorm.io/gorm"
)

type Transaction struct {
	Db   *gorm.DB
	Base *config.BaseDep
}

type TransactionRepo interface {
	CreateTransaction(t *model.Transaction) error
}

func NewTransactionRepo(repo *Transaction) TransactionRepo {
	return repo
}

func (r *Transaction) CreateTransaction(t *model.Transaction) error {
	err := r.Db.Create(&t).Error
	if err != nil {
		r.Base.Logger.Error("repo - failed create transaction", zap.Error(err))
		return err
	}

	return nil
}
