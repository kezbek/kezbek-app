package repository

import (
	"kezbek/db/model"
	"kezbek/internal/config"

	"gorm.io/gorm"
)

type Promo struct {
	Db   *gorm.DB
	Base *config.BaseDep
}

type PromoRepo interface {
	GetPromoAmount(totalProduct int64, totalPrice float64) float64
}

func NewPromoRepo(repo *Promo) PromoRepo {
	return repo
}

func (r *Promo) GetPromoAmount(totalProduct int64, totalPrice float64) float64 {
	var amount float64
	promo := model.Promo{
		Status: config.STATE_ACTICE,
	}

	err := r.Db.Where("total_product = ? AND total_price_min < ? and total_price_max > ?", totalProduct, totalPrice, totalPrice).First(&promo).Error
	if err != nil {
		r.Base.Logger.Error("repo - failed get promo amount")
		return 0
	}

	if promo.RewardPercentage != 0 {
		amount = promo.RewardPercentage * totalPrice / 100
	} else if promo.RewardAmount != 0 {
		amount = promo.RewardAmount
	}

	return amount
}
