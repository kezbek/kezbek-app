package repository

import (
	"kezbek/db/model"
	"kezbek/internal/config"

	"go.uber.org/zap"
	"gorm.io/gorm"
)

type Loyalty struct {
	Db   *gorm.DB
	Base *config.BaseDep
}

type LoyaltyRepo interface {
	GetDefaultLoyaltyTier() model.LoyaltyTier
	GetAllLoyatyTiers() (tiers []model.LoyaltyTier)
	GetLoyaltyReward(purchaseNo int32, tierID int32) (model.LoyaltyReward, error)
}

func NewLoyaltyRepo(repo *Loyalty) LoyaltyRepo {
	return repo
}

func (r *Loyalty) GetDefaultLoyaltyTier() model.LoyaltyTier {
	var tier model.LoyaltyTier

	r.Db.First(&tier).Order("weight asc")
	return tier
}

func (r *Loyalty) GetAllLoyatyTiers() (tiers []model.LoyaltyTier) {
	r.Db.Order("weight asc").Find(&tiers)
	r.Base.Logger.Info("TIERS", zap.Any("", tiers))
	return tiers
}

func (r *Loyalty) GetLoyaltyReward(purchaseNo int32, tierID int32) (model.LoyaltyReward, error) {
	reward := model.LoyaltyReward{
		TierID:         tierID,
		PurchaseNumber: purchaseNo,
	}

	err := r.Db.Where("tier_id = ? and purchase_number = ?", tierID, purchaseNo).First(&reward).Error

	if err != nil {
		r.Base.Logger.Error("repo - failed get reward", zap.Error(err))
		return reward, err
	}

	return reward, nil
}
