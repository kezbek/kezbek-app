package repository

import (
	"errors"
	"kezbek/db/model"
	"kezbek/internal/config"

	"go.uber.org/zap"
	"gorm.io/gorm"
)

type User struct {
	Db   *gorm.DB
	Base *config.BaseDep
}

type UserRepository interface {
	GetUserByID(id int64) (*model.User, error)
	GetUserByPhoneNo(phoneNo string) (*model.User, error)
	CreateUser(u *model.User) (*model.User, error)
	UpdateUser(u *model.User) error
}

func NewUserRepo(repo *User) UserRepository {
	return repo
}

func (r *User) GetUserByID(id int64) (user *model.User, err error) {
	err = r.Db.First(&user, id).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, nil
	} else if err != nil {
		r.Base.Logger.Error("failed to get user ", zap.Error(err))
		return nil, err
	}
	return user, nil
}

func (r *User) GetUserByPhoneNo(phoneNo string) (user *model.User, err error) {
	user = &model.User{
		PhoneNo: phoneNo,
	}

	err = r.Db.First(&user).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, nil
	} else if err != nil {
		r.Base.Logger.Error("failed to get user ", zap.Error(err))
		return nil, err
	}
	return user, nil
}

func (r *User) CreateUser(u *model.User) (*model.User, error) {
	err := r.Db.Create(&u).Error
	if err != nil {
		r.Base.Logger.Error("failed create user", zap.Error(err))
		return nil, err
	}

	return u, nil
}

func (r *User) UpdateUser(u *model.User) error {
	err := r.Db.Save(&u).Error
	if err != nil {
		r.Base.Logger.Error("failed update user", zap.Error(err))
		return err
	}

	return nil
}
