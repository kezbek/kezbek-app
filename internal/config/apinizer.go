package config

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/thoas/go-funk"
	"go.uber.org/zap"
)

type Context struct {
	Api             string
	Method          string
	MediaType       string
	IncomingReqTime int64
}

type BaseResponse struct {
	Data interface{}  `json:"data,omitempty"`
	Meta MetaResponse `json:"meta"`
}

type MetaResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func Request(logger LoggerConf) fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		start := time.Now().Unix()
		logger.Info("incoming request", zap.Any("", Context{
			Api:             ctx.OriginalURL(),
			Method:          ctx.Method(),
			MediaType:       ctx.Accepts(),
			IncomingReqTime: start,
		}))

		return ctx.Next()
	}
}

func BasicAuthMiddleware(logger LoggerConf) fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		partnerCode := strings.ToUpper(ctx.Get(REQ_HEADER_PARTNER))
		if !funk.Contains(AVAIL_PARTNER, partnerCode) {
			return ctx.Status(http.StatusUnauthorized).JSON(BaseResponse{
				Meta: MetaResponse{
					Code:    ERR_INVALID_PARTNER_CODE,
					Message: "invalid partner code",
				},
			})
		}

		username := os.Getenv(B2B_AUTH_PREFIX + partnerCode + "_USER")
		password := os.Getenv(B2B_AUTH_PREFIX + partnerCode + "_PASS")

		auth := ctx.Get(REQ_HEADER_AUTH)
		split := strings.Split(auth, REQ_HEADER_AUTH_BASIC)
		basic := split[1]

		logger.Info("DEBUG", zap.Any("split", split))
		logger.Info("DEBUG", zap.Any("basic", basic))

		authEncoded := base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", username, password)))
		if authEncoded != basic {
			return ctx.Status(http.StatusUnauthorized).JSON(BaseResponse{
				Meta: MetaResponse{
					Code:    ERR_WRONG_CREDENTIAL,
					Message: "wrong credentials",
				},
			})
		}

		return ctx.Next()
	}
}

// func Unauthorized(ctx *fiber.Ctx, message string) fiber.Handler {
// 	defMessage := "unauthorized"
// 	if message != "" {
// 		defMessage = message
// 	}
// 	return ctx.Status(http.StatusUnauthorized).JSON(BaseResponse{
// 		Message: defMessage,
// 	})
// }
