package config

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Logger struct {
	Dep *zap.Logger
}

type LoggerConf interface {
	Info(msg string, fields ...zapcore.Field)
	Error(msg string, fields ...zapcore.Field)
}

func NewLogger() LoggerConf {
	logConfig := zap.NewProductionConfig()
	logConfig.DisableStacktrace = true
	logConfig.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder

	log, _ := logConfig.Build()
	defer log.Sync()

	return &Logger{
		Dep: log,
	}
}

func (log *Logger) Info(msg string, fields ...zapcore.Field) {
	log.Dep.Info(msg, fields...)
}

func (log *Logger) Error(msg string, fields ...zapcore.Field) {
	log.Dep.Error(msg, fields...)
}
