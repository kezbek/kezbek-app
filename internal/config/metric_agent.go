package config

import "github.com/prometheus/client_golang/prometheus"

type MetricAgent struct {
	Histogram *prometheus.HistogramVec
	Counter   *prometheus.CounterVec
}

func NewMetricAgent(namespace string) *MetricAgent {
	histogramMetric := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: namespace,
		Buckets:   prometheus.DefBuckets,
		Name:      "histogram",
	}, []string{"layer", "function"})

	counterMetric := prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: namespace,
		Name:      "counter",
	}, []string{"layer", "function", "status"})

	return &MetricAgent{
		Histogram: histogramMetric,
		Counter:   counterMetric,
	}
}
