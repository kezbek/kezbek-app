package config

type BaseDep struct {
	Logger LoggerConf
	Metric *MetricAgent
}

func NewBaseDep(namespace string) *BaseDep {
	return &BaseDep{
		Logger: NewLogger(),
		Metric: NewMetricAgent(namespace),
	}
}
